<?php
/**
 * @var \CMain      $APPLICATION
 * @var \SimpleShop $shop
 */

if( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) {
    die();
}

require_once __DIR__ . '/install/index.php';

spl_autoload_register(function ($name) {
    $file = dirname(__DIR__) . '/' . str_replace('\\', '/', $name) . '.php';

    if( file_exists($file) ) {
        require_once $file;
    }
});
