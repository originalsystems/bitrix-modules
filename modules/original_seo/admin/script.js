window.Original     = window.Original || {};
window.Original.Seo = window.Original.Seo || (function() {
    "use strict";

    return {
        add: function(input_id, code, element) {
            document.getElementById(input_id).value += code;
        }
    };
})();