<?php
/**
 * @var \CMain $APPLICATION
 * @var \CUser $USER
 */

require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/prolog_admin_before.php';

if( $USER->IsAdmin() === false ) {
    return;
}

$module            = \original_seo\base\Module::instance();
$url               = $APPLICATION->GetCurPage();
$backUrl           = $_REQUEST['BACK_URL'] ? : '/';
$uri               = $_REQUEST['URI'] ? : '/';
$linkedElementCode = $_REQUEST['LINK_ELEMENT_CODE'] ? : NULL;
$components        = $_REQUEST['COMPONENTS'] ? : [];
$pageType          = $module->pageType($components);
$linkedElement     = $module->linkByCode($linkedElementCode);

$params = [
    'BACK_URL'          => $backUrl,
    'URI'               => $uri,
    'LINK_ELEMENT_CODE' => $linkedElementCode,
    'COMPONENTS'        => $components,
];

$action = $APPLICATION->GetCurPage() . '?' . http_build_query($params);

if( $linkedElement === NULL ) {
    $linkedElementCode = NULL;
}

$pageTitle = '';

switch( $pageType ) {
    case \original_seo\base\Module::PAGE_TYPE_STATIC:
        $pageTitle = 'Статичная страница';
        require __DIR__ . '/partial/static.php';
        break;

    case \original_seo\base\Module::PAGE_TYPE_CATALOG:
        $pageTitle = 'Страница каталога';
        require __DIR__ . '/partial/catalog.php';
        break;

    case \original_seo\base\Module::PAGE_TYPE_CATALOG_SECTION:
        $pageTitle = 'Страница раздела каталога';
        require __DIR__ . '/partial/catalog.section.php';
        break;

    case \original_seo\base\Module::PAGE_TYPE_CATALOG_ELEMENT:
        $pageTitle = 'Страница элемента каталога';
        require __DIR__ . '/partial/catalog.element.php';
        break;

    case \original_seo\base\Module::PAGE_TYPE_NEWS:
        $pageTitle = 'Страница новостей';
        require __DIR__ . '/partial/news.php';
        break;

    case \original_seo\base\Module::PAGE_TYPE_NEWS_DETAIL:
        $pageTitle = 'Страница новости детально';
        require __DIR__ . '/partial/news.detail.php';
        break;

    default:
        // nothing to do
        return;
}
?>
<script type="text/javascript">
    <?php if ($linkedElement !== NULL): ?>
    top.BX.WindowManager.Get().SetTitle('<?= $pageTitle; ?>, <?= addslashes($linkedElement['NAME']); ?>');
    <?php else: ?>
    top.BX.WindowManager.Get().SetTitle('<?= $pageTitle; ?>, по умолчанию');
    <?php endif; ?>
</script>

<?php require_once $_SERVER['DOCUMENT_ROOT'] . '/bitrix/modules/main/include/epilog_admin_after.php'; ?>
