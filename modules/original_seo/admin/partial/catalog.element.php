<?php
/**
 * @var \original_seo\base\Module $module
 * @var \CMain                    $APPLICATION
 * @var string                    $action
 * @var string                    $backUrl
 * @var string                    $uri
 * @var string                    $linkedElementCode
 * @var array                     $components
 * @var array                     $linkedElement
 */

$elementID = $components['bitrix:catalog.element']['ELEMENT_ID'];
$blockID   = $components['bitrix:catalog.element']['IBLOCK_ID'];
$groups    = [
    'ELEMENT' => [
        'LABEL'      => 'Элемент каталога',
        'PROPERTIES' => [],
    ],
];

$values = $module->config->getElement($blockID, $elementID, $linkedElement['ID']);

foreach( $module->elementPageProperties as $property ) {
    $id      = preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
        $module->menu->element($blockID, $inputID, 'Элемент каталога', 'ELEMENT'),
    ];

    $groups['ELEMENT']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[ELEMENT][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($values[$property]),
        'POPUP_MENU' => array_filter($menu),
    ];
}

if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $properties = (array)$_POST['PROPERTIES']['ELEMENT'];

    $module->config->setElement($properties, $blockID, $elementID, $linkedElement['ID']);

    LocalRedirect($backUrl);
}

require __DIR__ . '/_form.php';
