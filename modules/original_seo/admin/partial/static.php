<?php
/**
 * @var \original_seo\base\Module $module
 * @var \CMain                    $APPLICATION
 * @var string                    $action
 * @var string                    $backUrl
 * @var string                    $uri
 * @var string                    $linkedElementCode
 * @var array                     $components
 * @var array                     $linkedElement
 */

$groups = [
    'STATIC' => [
        'LABEL'      => 'Статичная страница',
        'PROPERTIES' => [],
    ],
];

$values = $module->config->getStatic($uri, $linkedElement['ID']);

foreach( $module->staticPageProperties as $property ) {
    $id      = preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
    ];

    $groups['STATIC']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[STATIC][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($values[$property]),
        'POPUP_MENU' => array_filter($menu),
    ];
}

if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $properties = (array)$_POST['PROPERTIES']['STATIC'];

    $module->config->setStatic($properties, $uri, $linkedElement['ID']);

    LocalRedirect($backUrl);
}

require __DIR__ . '/_form.php';
