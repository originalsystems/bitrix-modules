<?php
/**
 * @var \original_seo\base\Module $module
 * @var \CMain                    $APPLICATION
 * @var string                    $action
 * @var string                    $backUrl
 * @var string                    $uri
 * @var string                    $linkedElementCode
 * @var array                     $components
 * @var array                     $linkedElement
 */

$blockID   = $components['bitrix:catalog.section']['IBLOCK_ID'];
$sectionID = $components['bitrix:catalog.section']['SECTION_ID'];
$groups    = [
    'CURRENT'  => [
        'LABEL'      => 'Текущий раздел',
        'PROPERTIES' => [],
    ],
    'SECTIONS' => [
        'LABEL'      => 'Вложенные разделы',
        'PROPERTIES' => [],
    ],
    'ELEMENTS' => [
        'LABEL'      => 'Элементы каталога',
        'PROPERTIES' => [],
    ],
];

$currentValues  = $module->config->getSection($blockID, 'current', $sectionID, $linkedElement['ID']);
$sectionsValues = $module->config->getSection($blockID, 'sections', $sectionID, $linkedElement['ID']);
$elementsValues = $module->config->getSection($blockID, 'elements', $sectionID, $linkedElement['ID']);

foreach( $module->sectionPageProperties as $property ) {
    $id      = 'current-' . preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
        $module->menu->section($blockID, $inputID),
    ];

    $groups['CURRENT']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[CURRENT][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($currentValues[$property]),
        'POPUP_MENU' => $menu,
    ];
}

foreach( $module->sectionPageProperties as $property ) {
    $id      = 'sections-' . preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
        $module->menu->section($blockID, $inputID),
    ];

    $groups['SECTIONS']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[SECTIONS][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($sectionsValues[$property]),
        'POPUP_MENU' => $menu,
    ];
}

foreach( $module->elementPageProperties as $property ) {
    $id      = 'elements-' . preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
        $module->menu->element($blockID, $inputID, 'Элемент каталога', 'ELEMENT'),
    ];

    $groups['ELEMENTS']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[ELEMENTS][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($elementsValues[$property]),
        'POPUP_MENU' => $menu,
    ];
}

if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $properties = (array)$_POST['PROPERTIES'];

    $currentValues  = (array)$properties['CURRENT'];
    $sectionsValues = (array)$properties['SECTIONS'];
    $elementsValues = (array)$properties['ELEMENTS'];

    $module->config->setSection($currentValues, $blockID, 'current', $sectionID, $linkedElement['ID']);
    $module->config->setSection($sectionsValues, $blockID, 'sections', $sectionID, $linkedElement['ID']);
    $module->config->setSection($elementsValues, $blockID, 'elements', $sectionID, $linkedElement['ID']);

    LocalRedirect($backUrl);
}

require __DIR__ . '/_form.php';
