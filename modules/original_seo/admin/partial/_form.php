<?php
/**
 * @var \original_seo\base\Module $module
 * @var \CMain                    $APPLICATION
 * @var string                    $action
 * @var array                     $groups
 */

$aTabs = [];

foreach( $groups as $group => $groupConfig ) {
    $aTabs[] = [
        'DIV'   => 'original_seo_' . mb_strtolower($group),
        'TAB'   => $groupConfig['LABEL'],
        'ICON'  => 'main_settings',
        'TITLE' => $groupConfig['LABEL'],
    ];
}

$tabControl = new CAdminTabControl('originalSeoTabControl', $aTabs, true, true);
?>

<section class="section-seo-city-admin bx-core-adm-dialog-tabs">
    <form name="seo_city_form" method="POST" action="<?= $action; ?>" enctype="multipart/form-data">
        <?= bitrix_sessid_post() ?>
        <?php
        $tabControl->Begin();
        ?>

        <?php foreach( $groups as $group => $groupConfig ): ?>
            <?php $tabControl->BeginNextTab(); ?>
            <?php foreach( $groupConfig['PROPERTIES'] as $config ): ?>
                <tr>
                    <td class="adm-detail-content-cell-l" valign="top"><?= $config['LABEL']; ?>:</td>
                    <td class="adm-detail-content-cell-r" valign="top">
                        <?php if( preg_match('#_(TEXT|DESCRIPTION)$#', $config['LABEL']) === 1 ): ?>
                            <textarea name="<?= $config['NAME']; ?>"
                                      id="<?= $config['INPUT_ID']; ?>"
                                      data-role="input-selector-bind"
                                      cols="100"
                                      rows="4"><?= $config['VALUE']; ?></textarea>
                        <?php else: ?>
                            <input type="text"
                                   id="<?= $config['INPUT_ID']; ?>"
                                   data-role="input-selector-bind"
                                   size="100"
                                   name="<?= $config['NAME']; ?>"
                                   value="<?= $config['VALUE']; ?>">
                        <?php endif; ?>
                        <?php if( !empty($config['POPUP_MENU']) ): ?>
                            <?php
                            $popup = new CAdminPopupEx($config['MENU_ID'], $config['POPUP_MENU'], ['zIndex' => 2000]);
                            $popup->Show();
                            ?>
                            <input type="button" id="<?= $config['MENU_ID']; ?>" value="...">
                        <?php endif; ?>
                    </td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        <?php
        $tabControl->BeginNextTab();
        $tabControl->Buttons();
        ?>

        <input type="submit" value="Сохранить" title="Сохранить" class="adm-btn-save">
        <input type="reset" title="Отменить">
        <?php $tabControl->End(); ?>
    </form>
</section>
