<?php
/**
 * @var \original_seo\base\Module $module
 * @var \CMain                    $APPLICATION
 * @var string                    $action
 * @var string                    $backUrl
 * @var string                    $uri
 * @var string                    $linkedElementCode
 * @var array                     $components
 * @var array                     $linkedElement
 */

$blockID = $components['bitrix:news']['IBLOCK_ID'];
$groups  = [
    'CURRENT'  => [
        'LABEL'      => 'Страница новостей',
        'PROPERTIES' => [],
    ],
    'ELEMENTS' => [
        'LABEL'      => 'Новость детально',
        'PROPERTIES' => [],
    ],
];

$currentValues  = $module->config->getSection($blockID, 'current', NULL, $linkedElement['ID']);
$elementsValues = $module->config->getSection($blockID, 'elements', NULL, $linkedElement['ID']);

foreach( $module->staticPageProperties as $property ) {
    $id      = 'current-' . preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
    ];

    $groups['CURRENT']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[CURRENT][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($currentValues[$property]),
        'POPUP_MENU' => $menu,
    ];
}

foreach( $module->elementPageProperties as $property ) {
    $id      = 'elements-' . preg_replace('/\W+/', '-', mb_strtolower($property));
    $inputID = 'property-' . $id;
    $menuID  = 'menu-' . $id;
    $menu    = [
        $module->menu->element($module->linkedBlockID, $inputID, 'Связанный элемент', 'LINKED_ELEMENT'),
        $module->menu->element($blockID, $inputID, 'Новость', 'ELEMENT'),
    ];

    $groups['ELEMENTS']['PROPERTIES'][] = [
        'LABEL'      => $property,
        'NAME'       => "PROPERTIES[ELEMENTS][{$property}]",
        'INPUT_ID'   => $inputID,
        'MENU_ID'    => $menuID,
        'VALUE'      => htmlspecialcharsbx($elementsValues[$property]),
        'POPUP_MENU' => $menu,
    ];
}

if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $properties = (array)$_POST['PROPERTIES'];

    $currentValues  = (array)$properties['CURRENT'];
    $elementsValues = (array)$properties['ELEMENTS'];

    $module->config->setSection($currentValues, $blockID, 'current', NULL, $linkedElement['ID']);
    $module->config->setSection($elementsValues, $blockID, 'elements', NULL, $linkedElement['ID']);

    LocalRedirect($backUrl);
}

require __DIR__ . '/_form.php';
