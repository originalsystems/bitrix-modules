CREATE TABLE IF NOT EXISTS `original_seo_static` (
    `ID`                INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `LINKED_ELEMENT_ID` INT(11)      NULL,
    `URI`               VARCHAR(255) NOT NULL,
    `FIELD`             VARCHAR(64)  NOT NULL,
    `VALUE`             TEXT         NOT NULL
);

CREATE UNIQUE INDEX `original_seo_static_unique_fields`
    ON `original_seo_static` (`LINKED_ELEMENT_ID`, `URI`, `FIELD`) USING BTREE;

CREATE TABLE IF NOT EXISTS `original_seo_section` (
    `ID`                INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `LINKED_ELEMENT_ID` INT(11)     NULL,
    `IBLOCK_ID`         INT(11)     NOT NULL,
    `SECTION_ID`        INT(11)     NULL,
    `TYPE`              VARCHAR(16) NOT NULL,
    `FIELD`             VARCHAR(64) NOT NULL,
    `VALUE`             TEXT        NOT NULL
);

CREATE UNIQUE INDEX `original_seo_section_unique_fields`
    ON `original_seo_section` (`LINKED_ELEMENT_ID`, `IBLOCK_ID`, `SECTION_ID`, `TYPE`, `FIELD`) USING BTREE;

CREATE TABLE IF NOT EXISTS `original_seo_element` (
    `ID`                INT(11) UNSIGNED PRIMARY KEY AUTO_INCREMENT,
    `LINKED_ELEMENT_ID` INT(11)     NULL,
    `IBLOCK_ID`         INT(11)     NOT NULL,
    `ELEMENT_ID`        INT(11)     NOT NULL,
    `FIELD`             VARCHAR(64) NOT NULL,
    `VALUE`             TEXT        NOT NULL
);

CREATE UNIQUE INDEX `original_seo_element_unique_fields`
    ON `original_seo_element` (`LINKED_ELEMENT_ID`, `IBLOCK_ID`, `ELEMENT_ID`, `FIELD`) USING BTREE;
