<?php

if( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) {
    die();
}

class Original_Seo extends CModule
{
    const MODULE_ID          = 'original_seo';
    const MODULE_NAME        = 'Доменнозависимое СЕО';
    const MODULE_DESCRIPTION = 'Доменнозависимый сео модуль, распознающий активный город по текущему поддомену';

    public $MODULE_ID           = self::MODULE_ID;
    public $MODULE_NAME         = self::MODULE_NAME;
    public $MODULE_DESCRIPTION  = self::MODULE_DESCRIPTION;
    public $MODULE_VERSION      = '1.0.0';
    public $MODULE_VERSION_DATE = '2017-09-01';

    private static function isLocal()
    {
        $moduleID = self::MODULE_ID;

        return is_file($_SERVER['DOCUMENT_ROOT'] . "/local/modules/{$moduleID}/install/index.php");
    }

    private static function path()
    {
        $moduleID = self::MODULE_ID;

        return self::isLocal() ? '/local/modules/' . $moduleID : '/bitrix/modules/' . $moduleID;
    }

    private function links()
    {
        if( self::isLocal() ) {
            @mkdir($_SERVER['DOCUMENT_ROOT'] . '/local/components', BX_DIR_PERMISSIONS, true);

            return [
                '/local/components/original_seo'              => '../modules/original_seo/components',
                '/bitrix/admin/public_original_seo_tools.php' => '../../local/modules/original_seo/admin/public_tools.php',
            ];
        }

        return [
            '/bitrix/components/original_seo'             => '../modules/original_seo/components',
            '/bitrix/admin/public_original_seo_tools.php' => '../modules/original_seo/admin/public_tools.php',
        ];
    }

    public function DoInstall()
    {
        RegisterModule($this->MODULE_ID);

        foreach( $this->links() as $link => $target ) {
            @symlink($target, $_SERVER['DOCUMENT_ROOT'] . $link);
        }

        $eventManager = \Bitrix\Main\EventManager::getInstance();

        $eventManager->registerEventHandler('main', 'OnProlog', $this->MODULE_ID, get_class($this), 'OnProlog', 100000);
        $eventManager->registerEventHandler('main', 'OnEpilog', $this->MODULE_ID, get_class($this), 'OnEpilog');
        $eventManager->registerEventHandler('main', 'OnPanelCreate', $this->MODULE_ID, get_class($this), 'OnPanelCreate');

        $this->InstallDB();
    }

    public function InstallDB()
    {
        global $DB;

        $DB->RunSQLBatch(__DIR__ . '/install.sql');
    }

    public function UnInstallDB()
    {
        global $DB;

        $DB->RunSQLBatch(__DIR__ . '/uninstall.sql');
    }

    public function DoUninstall()
    {
        UnRegisterModule($this->MODULE_ID);

        foreach( $this->links() as $link => $target ) {
            @unlink($_SERVER['DOCUMENT_ROOT'] . $link);
        }

        $this->UnInstallDB();
    }

    public static function OnProlog()
    {
        global $APPLICATION;

        \Bitrix\Main\Page\Asset::getInstance()->addJs(self::path() . '/public/script.js');
        \Bitrix\Main\Page\Asset::getInstance()->addCss(self::path() . '/public/style.css');

        $APPLICATION = \original_seo\base\Decorator::instance($APPLICATION);
    }

    public static function OnEpilog()
    {
        /**
         * @var \CMain|\original_seo\base\Decorator $APPLICATION
         * @var \CUser                              $USER
         */
        global $APPLICATION, $USER;

        $file = $APPLICATION->GetCurPage(true);

        /**
         * Try work on unexpected pages like non-existing image or .css files
         */
        if( preg_match('#\.php$#', $file) === 0 ) {
            return;
        }

        /**
         * Not work on ajax
         */
        if( isset($_SERVER['HTTP_X_REQUESTED_WITH']) && $_SERVER['HTTP_X_REQUESTED_WITH'] === 'XMLHttpRequest' ) {
            return;
        }

        /**
         * Not work for bitrix ajax or admin pages
         */
        if( strpos($_SERVER['REQUEST_URI'], '/bitrix') === 0 ) {
            return;
        }

        /**
         * Not work on 404 error
         */
        if( defined('ERROR_404') ) {
            return;
        }

        \original_seo\base\Module::instance()->process();
    }

    public static function OnPanelCreate()
    {
        /**
         * @var \CMain|\original_seo\base\Decorator $APPLICATION
         * @var \CUser                              $USER
         */
        global $APPLICATION, $USER;

        if( $USER->IsAdmin() === false ) {
            return;
        }

        $module = \original_seo\base\Module::instance();

        \Bitrix\Main\Page\Asset::getInstance()->addJs(self::path() . '/admin/script.js');
        \Bitrix\Main\Page\Asset::getInstance()->addCss(self::path() . '/admin/style.css');

        CJSCore::Init(['admin_interface', 'window', 'ajax']);

        $params = [
            'URI'        => $module->uri(),
            'BACK_URL'   => $_SERVER['REQUEST_URI'],
            'COMPONENTS' => $module->components,
        ];
        $popup  = $APPLICATION->GetPopupLink([
            'URL'    => '/bitrix/admin/public_original_seo_tools.php?' . http_build_query($params),
            'PARAMS' => [
                'width'  => 920,
                'height' => 700,
                'resize' => false,
            ],
        ]);

        $APPLICATION->AddPanelButton([
            'HREF'      => 'javascript:' . $popup,
            'ID'        => 'original-seo-default-modal',
            'ICON'      => 'bx-panel-iblock-icon',
            'ALT'       => 'Сео по умолчанию',
            'TEXT'      => 'Сео по умолчанию',
            'MAIN_SORT' => '100000',
            'SORT'      => 1,
            'HINT'      => [
                'TITLE' => self::MODULE_NAME,
                'TEXT'  => self::MODULE_DESCRIPTION,
            ],
        ]);

        if( $module->linkElement !== NULL ) {
            $params = [
                'URI'               => $module->uri(),
                'BACK_URL'          => $_SERVER['REQUEST_URI'],
                'COMPONENTS'        => $module->components,
                'LINK_ELEMENT_CODE' => $module->linkElement['CODE'],
            ];
            $popup  = $APPLICATION->GetPopupLink([
                'URL'    => '/bitrix/admin/public_original_seo_tools.php?' . http_build_query($params),
                'PARAMS' => [
                    'width'  => 920,
                    'height' => 700,
                    'resize' => false,
                ],
            ]);

            $APPLICATION->AddPanelButton([
                'HREF'      => 'javascript:' . $popup,
                'ID'        => 'original-seo-domain-modal',
                'ICON'      => 'bx-panel-iblock-icon',
                'ALT'       => "Сео для элемента \"{$module->linkElement['NAME']}\"",
                'TEXT'      => "Сео для элемента \"{$module->linkElement['NAME']}\"",
                'MAIN_SORT' => '100000',
                'SORT'      => 2,
                'HINT'      => [
                    'TITLE' => self::MODULE_NAME,
                    'TEXT'  => self::MODULE_DESCRIPTION,
                ],
            ]);
        }
    }
}
