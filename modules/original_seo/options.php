<?php
/**
 * @var string $mid
 * @var \CMain $APPLICATION
 * @var \CUser $USER
 */

global $APPLICATION, $USER;

if( !defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true ) {
    die();
}

if( $USER->IsAdmin() === false ) {
    return;
}

$query  = http_build_query(array_merge($_GET, ['lang' => LANG, 'mid' => $mid]));
$url    = $APPLICATION->GetCurPage() . '?' . $query;
$arTabs = [
    [
        'DIV'   => 'tab1',
        'TAB'   => 'Настройка каталога',
        'TITLE' => 'Настройка каталога',
    ],
    [
        'DIV'   => 'tab2',
        'TAB'   => 'Настройка подстановок',
        'TITLE' => 'Настройка подстановок',
    ],
];

$types      = [];
$blocks     = [];
$typeResult = CIBlockType::GetList([], []);

while( $type = $typeResult->Fetch() ) {
    $typeBlocks  = [];
    $blockResult = CIBlock::GetList([], ['TYPE' => $type['ID']]);

    while( $block = $blockResult->Fetch() ) {
        $typeBlocks[]         = (int)$block['ID'];
        $blocks[$block['ID']] = [
            'ID'   => $block['ID'],
            'TYPE' => $type['ID'],
            'NAME' => $block['NAME'],
            'CODE' => $block['CODE'],
        ];
    }

    if( count($typeBlocks) > 0 ) {
        $types[$type['ID']] = $typeBlocks;
    }
}

$tabControl = new CAdminTabControl('tabControl', $arTabs);

$linkedBlockID   = (int)COption::GetOptionString($mid, 'LINKED_ELEMENT_IBLOCK_ID');
$linkedDefaultID = (int)COption::GetOptionString($mid, 'LINKED_ELEMENT_DEFAULT_ID');
$linkedElements  = [];

if( array_key_exists($linkedBlockID, $blocks) === false ) {
    $linkedBlockID   = NULL;
    $linkedDefaultID = NULL;
} else {
    $result = CIBlockElement::GetList([], [
        'IBLOCK_ID' => $linkedBlockID,
    ], false, false, [
        'ID',
        'IBLOCK_ID',
        'NAME',
        'CODE',
    ]);

    while( $row = $result->Fetch() ) {
        $linkedElements[$row['ID']] = $row['NAME'];
    }
}

$defaultProperties = [
    'H1',
    'TITLE',
    'DESCRIPTION',
    'KEYWORDS',
];

$pageProperties = [];

$pageProperties['SECTION_PAGE_PROPERTIES'] = @json_decode(\COption::GetOptionString($mid, 'SECTION_PAGE_PROPERTIES'), true);
$pageProperties['ELEMENT_PAGE_PROPERTIES'] = @json_decode(\COption::GetOptionString($mid, 'ELEMENT_PAGE_PROPERTIES'), true);
$pageProperties['STATIC_PAGE_PROPERTIES']  = @json_decode(\COption::GetOptionString($mid, 'STATIC_PAGE_PROPERTIES'), true);

$pageProperties['SECTION_PAGE_PROPERTIES'] = is_array($pageProperties['SECTION_PAGE_PROPERTIES']) ? $pageProperties['SECTION_PAGE_PROPERTIES'] : $defaultProperties;
$pageProperties['ELEMENT_PAGE_PROPERTIES'] = is_array($pageProperties['ELEMENT_PAGE_PROPERTIES']) ? $pageProperties['ELEMENT_PAGE_PROPERTIES'] : $defaultProperties;
$pageProperties['STATIC_PAGE_PROPERTIES']  = is_array($pageProperties['STATIC_PAGE_PROPERTIES']) ? $pageProperties['STATIC_PAGE_PROPERTIES'] : $defaultProperties;

if( $_SERVER['REQUEST_METHOD'] === 'POST' ) {
    $linkedBlockID   = $_POST['LINKED_ELEMENT_BLOCK_ID'];
    $linkedDefaultID = $_POST['LINKED_ELEMENT_DEFAULT_ID'];

    $pageProperties['SECTION_PAGE_PROPERTIES'] = array_unique(array_filter(array_map('trim', (array)$_POST['SECTION_PAGE_PROPERTIES'])));
    $pageProperties['ELEMENT_PAGE_PROPERTIES'] = array_unique(array_filter(array_map('trim', (array)$_POST['ELEMENT_PAGE_PROPERTIES'])));
    $pageProperties['STATIC_PAGE_PROPERTIES']  = array_unique(array_filter(array_map('trim', (array)$_POST['STATIC_PAGE_PROPERTIES'])));

    if( array_key_exists($linkedBlockID, $blocks) ) {
        COption::SetOptionString($mid, 'LINKED_ELEMENT_IBLOCK_ID', $linkedBlockID);
    } else {
        COption::SetOptionString($mid, 'LINKED_ELEMENT_IBLOCK_ID');
    }

    if( array_key_exists($linkedDefaultID, $linkedElements) ) {
        COption::SetOptionString($mid, 'LINKED_ELEMENT_DEFAULT_ID', $linkedDefaultID);
    } else {
        COption::SetOptionString($mid, 'LINKED_ELEMENT_DEFAULT_ID');
    }

    foreach( $pageProperties as $property => $values ) {
        if( !empty($values) ) {
            COption::SetOptionString($mid, $property, json_encode($values));
        } else {
            COption::SetOptionString($mid, $property);
        }
    }

    LocalRedirect($url);
}
?>
<style type="text/css">
    #original-seo-form .multiple-fields .fields {
        font-size:  0;
        min-height: 64px;
    }

    #original-seo-form .multiple-fields .fields input[type="text"] {
        display:   inline-block;
        margin:    0 5px 5px 0;
        font-size: 14px;
    }
</style>
<form id="original-seo-form" method="POST" action="<?= $url; ?>">
    <?= bitrix_sessid_post(); ?>
    <?php $tabControl->Begin(); ?>
    <?php $tabControl->BeginNextTab(); ?>
    <tr>
        <td width="35%" class="adm-detail-content-cell-l">Инфоблок с связанными элементами:</td>
        <td width="65%" class="adm-detail-content-cell-r">
            <select name="LINKED_ELEMENT_BLOCK_ID">
                <option value="">---</option>

                <?php foreach( $types as $type => $blockIDS ): ?>
                    <optgroup label="<?= $type; ?>">
                        <?php foreach( $blockIDS as $blockID ): ?>
                            <option value="<?= $blockID; ?>" <?= $blockID === $linkedBlockID ? 'selected' : ''; ?>><?= $blocks[$blockID]['NAME']; ?>
                                (ID: <?= $blocks[$blockID]['ID']; ?>)
                            </option>
                        <?php endforeach; ?>
                    </optgroup>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>

    <tr>
        <td width="35%" class="adm-detail-content-cell-l"></td>
        <td width="65%" class="adm-detail-content-cell-r" style="padding-bottom: 20px;">
            При заданном инфоблоке со связанными элементами по умолчанию элемент вычисляется из адреса страницы, на которой находится пользователь.
            Для корректной работы данного механизма необходимо, что бы поддомен соответствовал символьному коду элемента в инфоблоке со связанными элементами.
            Если данное поведение неприемлимо и необходима собственная логика по определению связанного элемента - необходимо задать глобальную переменную
            SEO_LINK_ELEMENT_ID в init.php файле и поместить в нее идентификатор связанного элемента, который соответствует текущему запросу.

            <br>
            <br>
            <code style="background: #e0e0e0; padding: 5px 10px;">$GLOBALS['SEO_LINK_ELEMENT_ID'] = 12;</code>
        </td>
    </tr>
    <tr>
        <td width="35%" class="adm-detail-content-cell-l">Связанный элемент по умолчанию:</td>
        <td width="65%" class="adm-detail-content-cell-r">
            <select name="LINKED_ELEMENT_DEFAULT_ID">
                <?php
                $defaultLabel = 'Выберите связанный элемент по умолчанию';

                if( empty($linkedBlockID) ) {
                    $defaultLabel = 'Необходимо выбрать инфоблок со связанными элементами';
                } elseif( empty($linkedElements) ) {
                    $defaultLabel = 'Нет ни одного активного элемента';
                }
                ?>
                <option value=""><?= $defaultLabel; ?></option>

                <?php foreach( $linkedElements as $id => $label ): ?>
                    <option value="<?= $id; ?>" <?= $linkedDefaultID === $id ? 'selected' : ''; ?>><?= $label; ?></option>
                <?php endforeach; ?>
            </select>
        </td>
    </tr>

    <?php $tabControl->BeginNextTab(); ?>

    <tr class="multiple-fields">
        <td width="35%" class="adm-detail-content-cell-l">
            Поля для страницы раздела
        </td>
        <td width="65%" class="adm-detail-content-cell-r" style="padding-bottom: 20px;">
            <div class="fields" data-field-group="section-page-properties">
                <?php foreach( $pageProperties['SECTION_PAGE_PROPERTIES'] as $property ): ?>
                    <input type="text" name="SECTION_PAGE_PROPERTIES[]" value="<?= htmlspecialcharsbx($property); ?>">
                <?php endforeach; ?>

                <input type="text" name="SECTION_PAGE_PROPERTIES[]" value="">
            </div>

            <input type="button" value="Добавить свойство" onclick="add_field('SECTION_PAGE_PROPERTIES[]', 'section-page-properties');">
        </td>
    </tr>

    <tr class="multiple-fields">
        <td width="35%" class="adm-detail-content-cell-l">
            Поля для страницы элемента
        </td>
        <td width="65%" class="adm-detail-content-cell-r" style="padding-bottom: 20px;">
            <div class="fields" data-field-group="element-page-properties">
                <?php foreach( $pageProperties['ELEMENT_PAGE_PROPERTIES'] as $property ): ?>
                    <input type="text" name="ELEMENT_PAGE_PROPERTIES[]" value="<?= htmlspecialcharsbx($property); ?>">
                <?php endforeach; ?>

                <input type="text" name="ELEMENT_PAGE_PROPERTIES[]" value="">
            </div>

            <input type="button" value="Добавить свойство" onclick="add_field('ELEMENT_PAGE_PROPERTIES[]', 'element-page-properties');">
        </td>
    </tr>

    <tr class="multiple-fields">
        <td width="35%" class="adm-detail-content-cell-l">
            Поля для статической страницы
        </td>
        <td width="65%" class="adm-detail-content-cell-r" style="padding-bottom: 20px;">
            <div class="fields" data-field-group="static-page-properties">
                <?php foreach( $pageProperties['STATIC_PAGE_PROPERTIES'] as $property ): ?>
                    <input type="text" name="STATIC_PAGE_PROPERTIES[]" value="<?= htmlspecialcharsbx($property); ?>">
                <?php endforeach; ?>

                <input type="text" name="STATIC_PAGE_PROPERTIES[]" value="">
            </div>

            <input type="button" value="Добавить свойство" onclick="add_field('STATIC_PAGE_PROPERTIES[]', 'static-page-properties');">
        </td>
    </tr>
    <?php $tabControl->Buttons(); ?>

    <input type="submit"
           name="Apply"
           value="Сохранить"
           title="Сохранить"
           class="adm-btn-save">

    <input type="reset" value="Сбросить">

    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="back_url_settings" value="<?= htmlspecialcharsbx($_REQUEST['back_url_settings']); ?>">
    <?php $tabControl->End(); ?>
</form>

<script type="text/javascript">
    function add_field(name, fields_group) {
        let input = document.createElement('input');

        input.type = 'text';
        input.name = name;

        document.querySelector('[data-field-group="' + fields_group + '"]').appendChild(input);
    }
</script>
