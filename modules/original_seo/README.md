# Bitrix module #

Seo module based on city checking right from url and compare it with selected iblock in bitrix

## Install ##
For correct work of module make a symlink right from vendor directory 
into /bitrix/modules or /local/modules directory

```bash
ln -s project_dir/vendor/ksaitechnologies/bitrix-module-seo-city project_dir/public_html/local/modules
```
