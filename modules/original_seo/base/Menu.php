<?php

namespace original_seo\base;

class Menu
{
    /**
     * @param int    $blockID
     * @param string $inputID
     * @param string $groupLabel
     * @param string $prefix
     * @param int    $depth
     *
     * @return array
     */
    public function element($blockID, $inputID, $groupLabel, $prefix, $depth = 0)
    {
        $module  = Module::instance();
        $blockID = (int)$blockID;

        if( $blockID <= 0 ) {
            return [];
        }

        $block = \CIBlock::GetByID($blockID)->Fetch() ? : NULL;

        if( $block === NULL ) {
            return [];
        }

        $menu = [
            'TEXT' => "{$groupLabel}, инфоблок \"{$block['NAME']}\"",
            'MENU' => [
                [
                    'TEXT'    => 'ID',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.ID#');",
                ],
                [
                    'TEXT'    => 'Название',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.NAME#');",
                ],
                [
                    'TEXT'    => 'Символьный код',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.CODE#');",
                ],
                [
                    'TEXT'    => 'Сортировка',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.SORT#');",
                ],
                [
                    'TEXT'    => 'Описание для анонса',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.PREVIEW_TEXT#');",
                ],
                [
                    'TEXT'    => 'Детальное описание',
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.DETAIL_TEXT#');",
                ],
            ],
        ];

        $properties = $module->elementProperties($blockID);

        if( count($properties) !== 0 ) {
            $propertiesMenu = [
                'TEXT' => 'Свойства',
                'MENU' => [],
            ];

            foreach( $properties as $code => $row ) {
                if( in_array($row['PROPERTY_TYPE'], ['N', 'S']) ) {
                    $propertiesMenu['MENU'][] = [
                        'TEXT'    => $row['NAME'],
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.PROPERTIES.{$code}#');",
                    ];
                } elseif( $row['PROPERTY_TYPE'] === 'E' && $depth === 0 ) {
                    $propertiesMenu['MENU'][] = $this->element($row['LINK_IBLOCK_ID'], $inputID, $row['NAME'], "{$prefix}.PROPERTIES.{$code}", $depth + 1);
                }
            }

            $menu['MENU'][] = $propertiesMenu;
        }

        $sectionMenu = $this->section($blockID, $inputID, $prefix . '.SECTION');

        if( !empty($sectionMenu) ) {
            $menu['MENU'][] = $sectionMenu;
        }

        return $menu;
    }

    /**
     * @param int    $blockID
     * @param string $inputID
     * @param string $prefix
     *
     * @return array
     */
    public function section($blockID, $inputID, $prefix = 'SECTION')
    {
        global $USER_FIELD_MANAGER;

        $menu = [];

        if( \CIBlockSection::GetList([], ['IBLOCK_ID' => $blockID])->SelectedRowsCount() > 0 ) {
            $menu = [
                'TEXT' => 'Секция',
                'MENU' => [
                    [
                        'TEXT'    => 'ID',
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.ID#');",
                    ],
                    [
                        'TEXT'    => 'Название',
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.NAME#');",
                    ],
                    [
                        'TEXT'    => 'Символьный код',
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.CODE#');",
                    ],
                    [
                        'TEXT'    => 'Сортировка',
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.SORT#');",
                    ],
                    [
                        'TEXT'    => 'Описание раздела',
                        'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.DESCRIPTION#');",
                    ],
                ],
            ];

            $sectionPropertiesMenu = [];

            $entityID = "IBLOCK_{$blockID}_SECTION";

            foreach( $USER_FIELD_MANAGER->GetUserFields($entityID) as $fieldName => $row ) {
                if( $row['MULTIPLE'] !== 'N' ) {
                    continue;
                }

                if( in_array($row['USER_TYPE_ID'], ['string', 'integer', 'double', 'datetime', 'date'], true) === false ) {
                    continue;
                }

                $name = $this->ufName($row['ID']);

                if( empty($name) ) {
                    continue;
                }

                $sectionPropertiesMenu[$fieldName] = [
                    'TEXT'    => $name,
                    'ONCLICK' => "Original.Seo.add('{$inputID}', '#{$prefix}.PROPERTIES.{$fieldName}#');",
                ];
            }

            if( count($sectionPropertiesMenu) > 0 ) {
                $menu['MENU'][] = [
                    'TEXT' => 'Свойства',
                    'MENU' => $sectionPropertiesMenu,
                ];
            }
        }

        return $menu;
    }

    private $ufNames = [];

    /**
     * @param int $userFieldID
     *
     * @return string
     */
    private function ufName($userFieldID)
    {
        /**
         * @var \CDatabase $DB
         */
        global $DB;

        $userFieldID = (int)$userFieldID;

        if( array_key_exists($userFieldID, $this->ufNames) === false ) {
            $sql = <<<SQL
SELECT * 
FROM b_user_field_lang
WHERE b_user_field_lang.USER_FIELD_ID = {$userFieldID}
SQL;

            $result = $DB->Query($sql)->Fetch();

            $this->ufNames[$userFieldID] = $result ? $result['EDIT_FORM_LABEL'] : NULL;
        }

        return $this->ufNames[$userFieldID];
    }
}
