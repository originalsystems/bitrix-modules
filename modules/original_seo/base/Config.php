<?php

namespace original_seo\base;

class Config
{
    /**
     * @param string $uri
     * @param int    $linkElementID
     *
     * @return array
     */
    public function getStatic($uri, $linkElementID = NULL)
    {
        global $DB;

        $linkElementID = (int)$linkElementID;
        $linkElementID = $linkElementID > 0 ? $linkElementID : NULL;

        $conditions   = [];
        $conditions[] = "`URI` = '" . $DB->db_Conn->real_escape_string($uri) . "'";

        if( $linkElementID === NULL ) {
            $conditions[] = '`LINKED_ELEMENT_ID` IS NULL';
        } else {
            $conditions[] = "`LINKED_ELEMENT_ID` = {$linkElementID}";
        }

        $where = implode(' AND ', $conditions);

        $sql = <<<SQL
SELECT * 
FROM `original_seo_static`
WHERE {$where}
SQL;

        $result = $DB->Query($sql);
        $config = [];

        while( $row = $result->Fetch() ) {
            $config[$row['FIELD']] = $row['VALUE'];
        }

        return $config;
    }

    /**
     * @param array  $properties
     * @param string $uri
     * @param int    $linkElementID
     */
    public function setStatic(array $properties, $uri, $linkElementID = NULL)
    {
        global $DB;

        $uri = $DB->db_Conn->real_escape_string($uri);

        $linkElementID        = (int)$linkElementID;
        $linkElementValue     = $linkElementID > 0 ? $linkElementID : 'NULL';
        $linkElementCondition = $linkElementID > 0 ? "`LINKED_ELEMENT_ID` = {$linkElementID}" : '`LINKED_ELEMENT_ID` IS NULL';

        foreach( $properties as $field => $value ) {
            $field = $DB->db_Conn->real_escape_string($field);
            $value = $DB->db_Conn->real_escape_string($value);

            $sql = <<<SQL
DELETE FROM `original_seo_static`
WHERE {$linkElementCondition} AND `URI` = '{$uri}' AND `FIELD` = '{$field}'
SQL;
            $DB->Query($sql);

            if( empty($value) === false ) {
                $sql = <<<SQL
INSERT INTO `original_seo_static` (`LINKED_ELEMENT_ID`, `URI`, `FIELD`, `VALUE`)
VALUES({$linkElementValue}, '{$uri}', '{$field}', '{$value}')
SQL;
                $DB->Query($sql);
            }
        }
    }

    /**
     * @param int    $blockID
     * @param string $type
     * @param int    $sectionID
     * @param int    $linkElementID
     *
     * @return array
     */
    public function getSection($blockID, $type, $sectionID = NULL, $linkElementID = NULL)
    {
        global $DB;

        $config  = [];
        $type    = $DB->db_Conn->real_escape_string($type);
        $blockID = (int)$blockID;

        if( $blockID <= 0 ) {
            return $config;
        }

        $linkElementID        = (int)$linkElementID;
        $linkElementCondition = $linkElementID > 0 ? "`LINKED_ELEMENT_ID` = {$linkElementID}" : '`LINKED_ELEMENT_ID` IS NULL';

        $sectionID        = (int)$sectionID;
        $sectionCondition = $sectionID > 0 ? "`SECTION_ID` = {$sectionID}" : '`SECTION_ID` IS NULL';

        $sql = <<<SQL
SELECT * 
FROM `original_seo_section`
WHERE `IBLOCK_ID` = {$blockID} AND {$linkElementCondition} AND {$sectionCondition} AND `TYPE` = '{$type}'
SQL;

        $result = $DB->Query($sql);

        while( $row = $result->Fetch() ) {
            $config[$row['FIELD']] = $row['VALUE'];
        }

        return $config;
    }

    /**
     * @param array  $properties
     * @param int    $blockID
     * @param string $type
     * @param int    $sectionID
     * @param int    $linkElementID
     */
    public function setSection(array $properties, $blockID, $type, $sectionID = NULL, $linkElementID = NULL)
    {
        global $DB;

        $type    = $DB->db_Conn->real_escape_string($type);
        $blockID = (int)$blockID;

        if( $blockID <= 0 ) {
            return;
        }

        $linkElementID        = (int)$linkElementID;
        $linkElementValue     = $linkElementID > 0 ? $linkElementID : 'NULL';
        $linkElementCondition = $linkElementID > 0 ? "`LINKED_ELEMENT_ID` = {$linkElementID}" : '`LINKED_ELEMENT_ID` IS NULL';

        $sectionID        = (int)$sectionID;
        $sectionValue     = $sectionID > 0 ? $sectionID : 'NULL';
        $sectionCondition = $sectionID > 0 ? "`SECTION_ID` = {$sectionID}" : '`SECTION_ID` IS NULL';

        foreach( $properties as $field => $value ) {
            $field = $DB->db_Conn->real_escape_string($field);
            $value = $DB->db_Conn->real_escape_string(trim($value));

            $sql = <<<SQL
DELETE FROM `original_seo_section`
WHERE `IBLOCK_ID` = {$blockID} AND {$linkElementCondition} AND {$sectionCondition} AND `TYPE` = '{$type}' AND `FIELD` = '{$field}'
SQL;

            $DB->Query($sql);

            if( empty($value) === false ) {
                $sql = <<<SQL
INSERT INTO `original_seo_section` (`LINKED_ELEMENT_ID`, `IBLOCK_ID`, `SECTION_ID`, `TYPE`, `FIELD`, `VALUE`)
VALUES ({$linkElementValue}, {$blockID}, {$sectionValue}, '{$type}', '{$field}', '{$value}')
SQL;

                $DB->Query($sql);
            }
        }
    }

    /**
     * @param int $blockID
     * @param int $elementID
     * @param int $linkElementID
     *
     * @return array
     */
    public function getElement($blockID, $elementID, $linkElementID)
    {
        global $DB;

        $config    = [];
        $blockID   = (int)$blockID;
        $elementID = (int)$elementID;

        if( $blockID <= 0 || $elementID <= 0 ) {
            return $config;
        }

        $linkElementID        = (int)$linkElementID;
        $linkElementCondition = $linkElementID > 0 ? "`LINKED_ELEMENT_ID` = {$linkElementID}" : '`LINKED_ELEMENT_ID` IS NULL';

        $sql = <<<SQL
SELECT *
FROM `original_seo_element`
WHERE IBLOCK_ID = {$blockID} AND `ELEMENT_ID` = {$elementID} AND {$linkElementCondition}
SQL;

        $result = $DB->Query($sql);

        while( $row = $result->Fetch() ) {
            $config[$row['FIELD']] = $row['VALUE'];
        }

        return $config;
    }

    /**
     * @param array $properties
     * @param int   $blockID
     * @param int   $elementID
     * @param int   $linkElementID
     */
    public function setElement(array $properties, $blockID, $elementID, $linkElementID)
    {
        global $DB;

        $blockID   = (int)$blockID;
        $elementID = (int)$elementID;

        if( $blockID <= 0 || $elementID <= 0 ) {
            return;
        }

        $linkElementID        = (int)$linkElementID;
        $linkElementValue     = $linkElementID > 0 ? $linkElementID : 'NULL';
        $linkElementCondition = $linkElementID > 0 ? "`LINKED_ELEMENT_ID` = {$linkElementID}" : '`LINKED_ELEMENT_ID` IS NULL';

        foreach( $properties as $field => $value ) {
            $field = $DB->db_Conn->real_escape_string($field);
            $value = $DB->db_Conn->real_escape_string(trim($value));

            $sql = <<<SQL
DELETE FROM `original_seo_element`
WHERE `IBLOCK_ID` = {$blockID} AND {$linkElementCondition} AND ELEMENT_ID = {$elementID} AND `FIELD` = '{$field}'
SQL;

            $DB->Query($sql);

            if( empty($value) === false ) {
                $sql = <<<SQL
INSERT INTO `original_seo_element` (`LINKED_ELEMENT_ID`, `IBLOCK_ID`, `ELEMENT_ID`, `FIELD`, `VALUE`)
VALUES ({$linkElementValue}, {$blockID}, {$elementID}, '{$field}', '{$value}')
SQL;

                $DB->Query($sql);
            }
        }
    }
}
