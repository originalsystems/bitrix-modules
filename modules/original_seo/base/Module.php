<?php

namespace original_seo\base;

class Module
{
    const PAGE_TYPE_STATIC          = 'static';
    const PAGE_TYPE_NEWS            = 'news';
    const PAGE_TYPE_NEWS_DETAIL     = 'news.detail';
    const PAGE_TYPE_CATALOG         = 'catalog';
    const PAGE_TYPE_CATALOG_SECTION = 'catalog.section';
    const PAGE_TYPE_CATALOG_ELEMENT = 'catalog.element';

    const SECTION_TYPE_CURRENT     = 'current';
    const SECTION_TYPE_SUBSECTIONS = 'subsections';
    const SECTION_TYPE_ELEMENTS    = 'elements';

    public $components = [];

    /**
     * @var \original_seo\base\Config
     */
    public $config;

    /**
     * @var \original_seo\base\Process
     */
    public $process;

    /**
     * @var \original_seo\base\Menu
     */
    public $menu;

    /**
     * @var \original_seo\base\Replace
     */
    public $replace;

    public $catalogElementComponents = [
        'bitrix:catalog.element',
    ];

    public $catalogSectionComponents = [
        'bitrix:catalog.section',
    ];

    public $catalogComponents = [
        'bitrix:catalog',
    ];

    public $newsComponents = [
        'bitrix:news',
    ];

    public $newsDetailComponents = [
        'bitrix:news.detail',
    ];

    public $sectionPageProperties = [];
    public $elementPageProperties = [];
    public $staticPageProperties  = [];

    /**
     * Configured options
     * @var int $linkedBlockID
     * @var int $linkedDefaultID
     */
    public $linkedBlockID;
    public $linkedDefaultID;

    /**
     * Calculated params
     * @var array  $linkBlock
     * @var array  $linkElement
     * @var string $subDomain
     */
    public $linkBlock;
    public $linkElement;
    public $subDomain;

    private function __construct()
    {
        $defaultProperties = [
            'H1',
            'TITLE',
            'DESCRIPTION',
            'KEYWORDS',
        ];

        $this->linkedBlockID   = (int)\COption::GetOptionString(\Original_Seo::MODULE_ID, 'LINKED_ELEMENT_IBLOCK_ID') ? : NULL;
        $this->linkedDefaultID = (int)\COption::GetOptionString(\Original_Seo::MODULE_ID, 'LINKED_ELEMENT_DEFAULT_ID') ? : NULL;

        $this->sectionPageProperties = $this->arrayOptions('SECTION_PAGE_PROPERTIES', $defaultProperties);
        $this->elementPageProperties = $this->arrayOptions('ELEMENT_PAGE_PROPERTIES', $defaultProperties);
        $this->staticPageProperties  = $this->arrayOptions('STATIC_PAGE_PROPERTIES', $defaultProperties);

        if( $this->linkedBlockID !== NULL ) {
            $this->linkBlock = \CIblock::GetList([], ['ID' => $this->linkedBlockID])->GetNext(false, false) ? : NULL;
        }

        $hostName = $_SERVER['HTTP_HOST'];
        $position = strpos($hostName, SITE_SERVER_NAME);

        $this->subDomain = in_array($position, [0, false], true) ? NULL : substr($hostName, 0, $position - 1);

        if( $this->linkBlock !== NULL ) {
            $filter = [
                'IBLOCK_ID' => $this->linkBlock['ID'],
            ];

            if( !empty($GLOBALS['SEO_LINK_ELEMENT_ID']) && is_numeric($GLOBALS['SEO_LINK_ELEMENT_ID']) ) {
                /**
                 * Search global defined link element
                 */

                $filter['ID'] = $GLOBALS['SEO_LINK_ELEMENT_ID'];

                $this->linkElement = \CIBlockElement::GetList([], $filter)->Fetch() ? : NULL;
            } elseif( $element = $this->linkByCode($this->subDomain) ) {
                /**
                 * Search link element by subdomain
                 */

                $this->linkElement = $element;
            } elseif( $this->linkedDefaultID !== NULL ) {
                /**
                 * Search default link element
                 */
                $filter['ID'] = $this->linkedDefaultID;

                $this->linkElement = \CIBlockElement::GetList([], $filter)->Fetch() ? : NULL;
            }
        }

        $this->config  = new Config();
        $this->process = new Process();
        $this->menu    = new Menu();
        $this->replace = new Replace();
    }

    /**
     * @param string $name
     * @param array  $default
     *
     * @return array|mixed
     */
    private function arrayOptions($name, array $default = [])
    {
        $value = @json_decode(\COption::GetOptionString(\Original_Seo::MODULE_ID, $name), true);

        return is_array($value) ? $value : $default;
    }

    /**
     * @return \original_seo\base\Module
     */
    public static function instance()
    {
        static $instance;

        if( $instance === NULL ) {
            $instance = new self();
        }

        return $instance;
    }

    /**
     * @param array $components
     *
     * @return string
     */
    public function pageType(array $components)
    {
        if( array_key_exists('bitrix:news', $components) ) {
            if( array_key_exists('bitrix:news.detail', $components) ) {
                return self::PAGE_TYPE_NEWS_DETAIL;
            }

            return self::PAGE_TYPE_NEWS;
        }

        if( array_key_exists('bitrix:catalog', $components) ) {
            if( array_key_exists('bitrix:catalog.element', $components) ) {
                return self::PAGE_TYPE_CATALOG_ELEMENT;
            }

            if( array_key_exists('bitrix:catalog.section', $components) ) {
                return self::PAGE_TYPE_CATALOG_SECTION;
            }

            return self::PAGE_TYPE_CATALOG;
        }

        return self::PAGE_TYPE_STATIC;
    }

    /**
     * @param string $code
     *
     * @return array
     */
    public function linkByCode($code)
    {
        if( $this->linkBlock === NULL ) {
            return NULL;
        }

        if( empty($code) ) {
            return NULL;
        }

        return \CIBlockElement::GetList([], [
            'IBLOCK_ID' => $this->linkBlock['ID'],
            'CODE'      => $code,
        ])->Fetch() ? : NULL;
    }

    /**
     * @param string $uri
     *
     * @return mixed|null
     */
    public function uri($uri = NULL)
    {
        if( $uri === NULL ) {
            $uri = $_SERVER['REQUEST_URI'];
        }

        $uri = preg_replace('#//+#', '/', '/' . $uri);
        $uri = parse_url($uri, PHP_URL_PATH);
        $uri = preg_replace('#index\.php$#', '', $uri);

        return $uri;
    }

    /**
     * Calc replace properties by page type and replace values
     */
    public function process()
    {
        /**
         * @var \CMain $APPLICATION
         */
        global $APPLICATION;

        switch( $this->pageType($this->components) ) {
            case self::PAGE_TYPE_STATIC:
                list($properties, $replace) = $this->process->forStatic();
                break;

            case self::PAGE_TYPE_CATALOG:
                list($properties, $replace) = $this->process->forCatalog();
                break;

            case self::PAGE_TYPE_CATALOG_SECTION:
                list($properties, $replace) = $this->process->forCatalogSection();
                break;

            case self::PAGE_TYPE_CATALOG_ELEMENT:
                list($properties, $replace) = $this->process->forCatalogElement();
                break;

            case self::PAGE_TYPE_NEWS:
                list($properties, $replace) = $this->process->forNews();
                break;

            case self::PAGE_TYPE_NEWS_DETAIL:
                list($properties, $replace) = $this->process->forNewsDetail();
                break;

            default:
                return;
        }

        $content = ob_get_clean();

        foreach( $properties as $property => $value ) {
            $value = strtr($value, $replace);
            $value = preg_replace('/#(ELEMENT|SECTION|LINKED_ELEMENT)[^#]+?#/', '', $value);

            if( $property === 'H1' ) {
                $APPLICATION->SetTitle($value, []);

                $content = preg_replace('#<h1([^>]*?)>(.+?)</h1>#', '<h1$1>' . $APPLICATION->GetTitle() . '</h1>', $content);
            } else {
                $APPLICATION->SetPageProperty($property, $value);
            }
        }

        ob_start();

        echo $content;
    }

    /**
     * @param int $blockID
     *
     * @return array
     */
    public function elementProperties($blockID)
    {
        static $properties = [];

        if( array_key_exists($blockID, $properties) === false ) {
            $properties[$blockID] = [];

            $result = \CIBlockProperty::GetList([], [
                'IBLOCK_ID' => $blockID,
                'MULTIPLE'  => 'N',
                'ACTIVE'    => 'Y',
                '!CODE'     => false,
            ]);

            while( $row = $result->Fetch() ) {
                if( in_array($row['PROPERTY_TYPE'], ['S', 'N', 'E'], true) ) {
                    $properties[$blockID][mb_strtoupper($row['CODE'])] = $row;
                }
            }
        }

        return $properties[$blockID];
    }
}
