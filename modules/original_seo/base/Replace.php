<?php

namespace original_seo\base;

class Replace
{
    /**
     * @param int    $blockID
     * @param int    $elementID
     * @param string $prefix
     * @param int    $depth
     *
     * @return string[]
     */
    public function element($blockID, $elementID, $prefix, $depth = 0)
    {
        $module    = Module::instance();
        $blockID   = (int)$blockID;
        $elementID = (int)$elementID;

        if( $blockID <= 0 || $elementID <= 0 ) {
            return [];
        }

        $block = \CIBlock::GetByID($blockID)->Fetch() ? : NULL;

        if( $block === NULL ) {
            return [];
        }

        $element = \CIBlockElement::GetList([], ['ID' => $elementID, 'IBLOCK_ID' => $blockID])->GetNext(false, true) ? : NULL;

        if( $element === NULL ) {
            return [];
        }

        $replace = [];

        $replace["#{$prefix}.ID#"]           = $element['ID'];
        $replace["#{$prefix}.NAME#"]         = $element['NAME'];
        $replace["#{$prefix}.CODE#"]         = $element['CODE'];
        $replace["#{$prefix}.SORT#"]         = $element['SORT'];
        $replace["#{$prefix}.PREVIEW_TEXT#"] = $element['~PREVIEW_TEXT'];
        $replace["#{$prefix}.DETAIL_TEXT#"]  = $element['~DETAIL_TEXT'];

        $properties        = $module->elementProperties($blockID);
        $elementProperties = \CIBlockElement::GetPropertyValues($blockID, ['ID' => $elementID])->Fetch();

        foreach( $properties as $code => $property ) {
            $value = $elementProperties[$property['ID']];

            if( $property['PROPERTY_TYPE'] === 'S' ) {
                $replace["#{$prefix}.PROPERTIES.{$code}#"] = $value;
            } elseif( $property['PROPERTY_TYPE'] === 'N' ) {
                $value = number_format($value, 2, '.', '');
                $value = str_replace('.00', '', $value);

                $replace["#{$prefix}.PROPERTIES.{$code}#"] = $value;
            } elseif( $property['PROPERTY_TYPE'] === 'E' && $depth === 0 ) {
                $replace = array_merge($replace, $this->element($property['LINK_IBLOCK_ID'], $value, "{$prefix}.PROPERTIES.{$code}", $depth + 1));
            }
        }

        $replace = array_merge($replace, $this->section($blockID, $element['IBLOCK_SECTION_ID'], "{$prefix}.SECTION"));

        return $replace;
    }

    public function section($blockID, $sectionID, $prefix)
    {
        /**
         * @var \CUserTypeManager $USER_FIELD_MANAGER
         */
        global $USER_FIELD_MANAGER;

        $module    = Module::instance();
        $blockID   = (int)$blockID;
        $sectionID = (int)$sectionID;
        $replace   = [];

        if( $blockID <= 0 || $sectionID <= 0 ) {
            return $replace;
        }

        $section = \CIBlockSection::GetList([], ['IBLOCK_ID' => $blockID, 'ID' => $sectionID])->GetNext(false, true) ? : NULL;

        if( $section !== NULL ) {
            $replace["#{$prefix}.ID#"]          = $section['ID'];
            $replace["#{$prefix}.NAME#"]        = $section['NAME'];
            $replace["#{$prefix}.CODE#"]        = $section['CODE'];
            $replace["#{$prefix}.SORT#"]        = $section['SORT'];
            $replace["#{$prefix}.DESCRIPTION#"] = $section['~DESCRIPTION'];

            $entityID = "IBLOCK_{$blockID}_SECTION";

            foreach( $USER_FIELD_MANAGER->GetUserFields($entityID) as $fieldName => $row ) {
                if( $row['MULTIPLE'] === 'N' && in_array($row['USER_TYPE_ID'], ['string', 'integer', 'double', 'datetime', 'date'], true) ) {
                    $replace["#{$prefix}.PROPERTIES.{$fieldName}#"] = $USER_FIELD_MANAGER->GetUserFieldValue($entityID, $fieldName, $section['ID']);
                }
            }
        }

        return $replace;
    }
}
