<?php

namespace original_seo\base;

class Process
{
    public function forStatic()
    {
        $module     = Module::instance();
        $properties = [];
        $replace    = [];

        $uri        = $module->uri();
        $properties = array_merge($properties, $module->config->getStatic($uri));
        $properties = array_merge($properties, $module->config->getStatic($uri, $module->linkElement['ID']));

        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $module->linkElement['ID'], 'LINKED_ELEMENT'));

        return [$properties, $replace];
    }

    public function forCatalog()
    {
        $module        = Module::instance();
        $blockID       = $module->components['bitrix:catalog']['IBLOCK_ID'];
        $linkElementID = $module->linkElement['ID'] ? : NULL;

        $properties = [];
        $replace    = [];

        $properties = array_merge($properties, $module->config->getSection($blockID, 'current', NULL, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'current', NULL, $module->linkElement['ID']));
        }

        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $module->linkElement['ID'], 'LINKED_ELEMENT'));

        return [$properties, $replace];
    }

    public function forCatalogSection()
    {
        $module        = Module::instance();
        $blockID       = $module->components['bitrix:catalog.section']['IBLOCK_ID'];
        $sectionID     = $module->components['bitrix:catalog.section']['SECTION_ID'];
        $linkElementID = $module->linkElement['ID'] ? : NULL;

        $properties = [];
        $replace    = [];

        $parentSections  = [];
        $parentSectionID = $sectionID;

        while( true ) {
            $section = \CIBlockSection::GetList([], [
                'ID'        => $parentSectionID,
                'IBLOCK_ID' => $blockID,
            ])->Fetch() ? : NULL;

            $parentSectionID = $parentSections[] = (int)$section['IBLOCK_SECTION_ID'] ? : NULL;

            if( $parentSectionID === NULL ) {
                break;
            }
        }

        $parentSections = array_reverse($parentSections);

        foreach( $parentSections as $parentSectionID ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'sections', $parentSectionID, NULL));

            if( $linkElementID > 0 ) {
                $properties = array_merge($properties, $module->config->getSection($blockID, 'sections', $parentSectionID, $linkElementID));
            }
        }

        $properties = array_merge($properties, $module->config->getSection($blockID, 'current', $sectionID, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'current', $sectionID, $linkElementID));
        }

        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $module->linkElement['ID'], 'LINKED_ELEMENT'));
        $replace = array_merge($replace, $module->replace->section($blockID, $sectionID, 'SECTION'));

        return [$properties, $replace];
    }

    public function forCatalogElement()
    {
        $module        = Module::instance();
        $blockID       = $module->components['bitrix:catalog.element']['IBLOCK_ID'];
        $elementID     = $module->components['bitrix:catalog.element']['ELEMENT_ID'];
        $linkElementID = $module->linkElement['ID'] ? : NULL;

        $properties = [];
        $replace    = [];

        $element = \CIBlockElement::GetList([], [
            'ID'        => $elementID,
            'IBLOCK_ID' => $blockID,
        ])->Fetch() ? : NULL;

        $parentSections  = [];
        $parentSectionID = $element['IBLOCK_SECTION_ID'];

        while( true ) {
            $section = \CIBlockSection::GetList([], [
                'ID'        => $parentSectionID,
                'IBLOCK_ID' => $blockID,
            ])->Fetch() ? : NULL;

            $parentSectionID = $parentSections[] = (int)$section['IBLOCK_SECTION_ID'] ? : NULL;

            if( $parentSectionID === NULL ) {
                break;
            }
        }

        $parentSections = array_reverse($parentSections);

        foreach( $parentSections as $parentSectionID ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'elements', $parentSectionID, NULL));

            if( $linkElementID > 0 ) {
                $properties = array_merge($properties, $module->config->getSection($blockID, 'elements', $parentSectionID, $linkElementID));
            }
        }

        $properties = array_merge($properties, $module->config->getElement($blockID, $elementID, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getElement($blockID, $elementID, $linkElementID));
        }

        $replace = array_merge($replace, $module->replace->element($blockID, $elementID, 'ELEMENT'));
        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $linkElementID, 'LINKED_ELEMENT'));

        return [$properties, $replace];
    }

    public function forNews()
    {
        $module        = Module::instance();
        $blockID       = $module->components['bitrix:news']['IBLOCK_ID'];
        $linkElementID = $module->linkElement['ID'] ? : NULL;

        $properties = [];
        $replace    = [];

        $properties = array_merge($properties, $module->config->getSection($blockID, 'current', NULL, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'current', NULL, $module->linkElement['ID']));
        }

        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $module->linkElement['ID'], 'LINKED_ELEMENT'));

        return [$properties, $replace];
    }

    public function forNewsDetail()
    {
        $module        = Module::instance();
        $blockID       = $module->components['bitrix:news.detail']['IBLOCK_ID'];
        $elementID     = $module->components['bitrix:news.detail']['ELEMENT_ID'];
        $linkElementID = $module->linkElement['ID'] ? : NULL;

        $properties = [];
        $replace    = [];

        $properties = array_merge($properties, $module->config->getSection($blockID, 'elements', NULL, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getSection($blockID, 'elements', NULL, $linkElementID));
        }

        $properties = array_merge($properties, $module->config->getElement($blockID, $elementID, NULL));

        if( $linkElementID > 0 ) {
            $properties = array_merge($properties, $module->config->getElement($blockID, $elementID, $linkElementID));
        }

        $replace = array_merge($replace, $module->replace->element($blockID, $elementID, 'ELEMENT'));
        $replace = array_merge($replace, $module->replace->element($module->linkBlock['ID'], $linkElementID, 'LINKED_ELEMENT'));

        return [$properties, $replace];
    }
}
