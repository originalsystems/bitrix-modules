<?php

namespace original_seo\base;

class Decorator
{
    /**
     * @var \CMain
     */
    private static $APPLICATION;

    private function __construct($APPLICATION)
    {
        self::$APPLICATION = $APPLICATION;
    }

    /**
     * @param \CMain $APPLICATION
     *
     * @return \original_seo\base\Decorator
     */
    public static function instance($APPLICATION)
    {
        static $instance;

        if( $instance === NULL ) {
            $instance = new self($APPLICATION);
        }

        return $instance;
    }

    public function __get($name)
    {
        return self::$APPLICATION->{$name};
    }

    public function __set($name, $value)
    {
        self::$APPLICATION->{$name} = $value;
    }

    public function __isset($name)
    {
        return isset(self::$APPLICATION->{$name});
    }

    public function __unset($name)
    {
        unset(self::$APPLICATION->{$name});
    }

    public function __call($name, $arguments)
    {
        return call_user_func_array([self::$APPLICATION, $name], $arguments);
    }

    public function IncludeComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module = Module::instance();

        if( in_array($componentName, $module->catalogComponents, true) ) {
            return $this->IncludeCatalogComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
        }

        if( in_array($componentName, $module->catalogSectionComponents, true) ) {
            return $this->IncludeCatalogSectionComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
        }

        if( in_array($componentName, $module->catalogElementComponents, true) ) {
            return $this->IncludeCatalogElementComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
        }

        if( in_array($componentName, $module->newsComponents, true) ) {
            return $this->IncludeNewsComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
        }

        if( in_array($componentName, $module->newsDetailComponents, true) ) {
            return $this->IncludeNewsDetailComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
        }

        return self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);
    }

    private function IncludeCatalogComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module  = Module::instance();
        $blockID = (int)$arParams['IBLOCK_ID'];
        $result  = self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);

        $module->components['bitrix:catalog'] = [
            'IBLOCK_ID' => $blockID,
        ];

        return $result;
    }

    private function IncludeCatalogSectionComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module    = Module::instance();
        $blockID   = (int)$arParams['IBLOCK_ID'];
        $sectionID = self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);

        $module->components['bitrix:catalog.section'] = [
            'IBLOCK_ID'  => $blockID,
            'SECTION_ID' => (int)$sectionID,
        ];

        return $sectionID;
    }

    private function IncludeCatalogElementComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module    = Module::instance();
        $blockID   = (int)$arParams['IBLOCK_ID'];
        $elementID = self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);

        $module->components['bitrix:catalog.element'] = [
            'IBLOCK_ID'  => $blockID,
            'ELEMENT_ID' => (int)$elementID,
        ];

        return $elementID;
    }

    private function IncludeNewsComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module  = Module::instance();
        $blockID = (int)$arParams['IBLOCK_ID'];
        $result  = self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);

        $module->components['bitrix:news'] = [
            'IBLOCK_ID' => $blockID,
        ];

        return $result;
    }

    private function IncludeNewsDetailComponent($componentName, $componentTemplate, $arParams = [], $parentComponent = NULL, $arFunctionParams = [])
    {
        $module    = Module::instance();
        $blockID   = (int)$arParams['IBLOCK_ID'];
        $elementID = self::$APPLICATION->IncludeComponent($componentName, $componentTemplate, $arParams, $parentComponent, $arFunctionParams);

        $module->components['bitrix:news.detail'] = [
            'IBLOCK_ID'  => $blockID,
            'ELEMENT_ID' => (int)$elementID,
        ];

        return $elementID;
    }
}
